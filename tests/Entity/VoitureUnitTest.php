<?php

namespace App\Tests\Entity;


use App\Entity\Voiture;
use PHPUnit\Framework\TestCase;
use App\Tests\DateTime;

class VoitureUnitTest extends TestCase
{
    public function testSomething(): void
    {
        $this->assertTrue(true);
    }
     public function testEntityVoiture(): void
    {
        $voiture=new Voiture();
        $this->assertInstanceOf(Voiture::class, $voiture);
    }

    public function testIsTrueVoiture()
    {
        $voiture = new Voiture();
        $voiture->setSerie("209TU4365") 
                ->setDateMiseEnMarche ('2020-01-10')
                ->setModele('MB')
                ->setPrixJour(20.5);

        $this->assertTrue($voiture ->getSerie() === "209TU4365"); 
        $this->assertTrue($voiture ->getDateMiseEnMarche() === '2020-01-10'); 
        $this->assertTrue($voiture->getModele() === 'MB');
        $this->assertTrue($voiture->getPrixJour() === 20.5);
    }

    public function testIsFalseVoiture()
    {
        $datetime = new \DateTime();
        $voiture = new Voiture();
        $voiture->setSerie("209TU4365") 
                ->setDateMiseEnMarche ('2020-01-10')
                ->setModele('MB')
                ->setPrixJour(20.5);

        $this->assertFalse($voiture ->getSerie() === "209TU9965"); 
        $this->assertFalse($voiture ->getDateMiseEnMarche() === '2020-12-12'); 
        $this->assertFalse($voiture->getModele() === 'BMW');
        $this->assertFalse($voiture->getPrixJour() === 70.5);
    }
}

