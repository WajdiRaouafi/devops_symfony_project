<?php

namespace App\Test\Controller;

use App\Entity\Client;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ClientControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private ClientRepository $repository;
    private string $path = '/client/';

    protected function setUp(): void
    {
        $this->client = static::createClient();
        $this->repository = (static::getContainer()->get('doctrine'))->getRepository(Client::class);

        foreach ($this->repository->findAll() as $object) {
            $this->repository->remove($object, true);
        }
    }

    public function testIndex(): void
    {
        $crawler = $this->client->request('GET', $this->path);

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Client index');

        // Use the $crawler to perform additional assertions e.g.
        // self::assertSame('Some text on the page', $crawler->filter('.p')->first());
    }

    // public function testNew(): void
    // {
    //     $originalNumObjectsInRepository = count($this->repository->findAll());

    //     $this->markTestIncomplete();
    //     $this->client->request('GET', sprintf('%snew', $this->path));

    //     self::assertResponseStatusCodeSame(200);

    //     $this->client->submitForm('Save', [
    //         'client[nom]' => 'Testing',
    //         'client[prenom]' => 'Testing',
    //         'client[cin]' => 'Testing',
    //         'client[adresse]' => 'Testing',
    //     ]);

    //     self::assertResponseRedirects('/client/');

    //     self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));
    // }


    public function testNew()
    {
        // $client = static::createClient();
        // $client->followRedirects();
        $crawler = $this->client->request('GET', '/client/new');
        $this->assertResponseIsSuccessful();


        // $buttonCrawlerNode = $crawler->selectButton('Save');

        // $form = $buttonCrawlerNode->form();

        // $uuid = uniqid();

        $form = $crawler->selectButton('Save')->form([
            'client[nom]' => 'Raouafi' . uniqid(),
            'client[prenom]' => 'Wajdi',
            'client[cin]' => 15004838,
            'client[adresse]' => 'Tunis',
        ]);

        $this->client->submit($form);
        $crawler = $this->client->followRedirect();  
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'Raouafi');
    }

    // public function testAddNewClient()
    // {
    //     $client = static::createClient();
    //     $client->followRedirects();
    //     $crawler = $client->request('GET', '/client/new');

    //     $buttonCrawlerNode = $crawler->selectButton('Save');

    //     $form = $buttonCrawlerNode->form();

    //     $uuid = uniqid();

    //     $form = $buttonCrawlerNode->form([
    //         'client[nom]' => 'Raouafi' . $uuid,
    //         'client[prenom]' => 'Wajdi',
    //         'client[cin]' => 15004838,
    //         'client[adresse]' => 'Tunis',
    //     ]);

    //     $client->submit($form);

    //     $this->assertResponseIsSuccessful();
    //     $this->assertSelectorTextContains('body', 'Add Demo For Test' . $uuid);
    // }


    public function testShow(): void
    {
        // $this->markTestIncomplete();
        $fixture = new Client();
        $fixture->setNom('Raouafi');
        $fixture->setPrenom('Wajdi');
        $fixture->setCin(15004838);
        $fixture->setAdresse('Tunis');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));

        self::assertResponseStatusCodeSame(200);
        self::assertPageTitleContains('Client');

        // Use assertions to check that the properties are properly displayed.
    }

    public function testEdit(): void
    {
        // $this->markTestIncomplete();
        $fixture = new Client();
        $fixture->setNom('Med');
        $fixture->setPrenom('Med');
        $fixture->setCin(00000000);
        $fixture->setAdresse('TestAdd');

        $this->repository->add($fixture, true);

        $this->client->request('GET', sprintf('%s%s/edit', $this->path, $fixture->getId()));

        $this->client->submitForm('Update', [
            'client[nom]' => 'Raouafi',
            'client[prenom]' => 'Wajdi',
            'client[cin]' => 15004838,
            'client[adresse]' => 'Tunis',
        ]);

        self::assertResponseRedirects('/client/');

        $fixture = $this->repository->findAll();

        self::assertSame('Raouafi', $fixture[0]->getNom());
        self::assertSame('Wajdi', $fixture[0]->getPrenom());
        self::assertSame(15004838, $fixture[0]->getCin());
        self::assertSame('Tunis', $fixture[0]->getAdresse());
    }

    public function testRemove(): void
    {
        // $this->markTestIncomplete();

        $originalNumObjectsInRepository = count($this->repository->findAll());

        $fixture = new Client();
        $fixture->setNom('Raouafi');
        $fixture->setPrenom('Wajdi');
        $fixture->setCin(15004838);
        $fixture->setAdresse('Tunis');

        $this->repository->add($fixture, true);

        self::assertSame($originalNumObjectsInRepository + 1, count($this->repository->findAll()));

        $this->client->request('GET', sprintf('%s%s', $this->path, $fixture->getId()));
        $this->client->submitForm('Delete');

        self::assertSame($originalNumObjectsInRepository, count($this->repository->findAll()));
        self::assertResponseRedirects('/client/');
    }
}
